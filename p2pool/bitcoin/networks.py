import os
import platform

from twisted.internet import defer

from . import data
from p2pool.util import math, pack, jsonrpc

@defer.inlineCallbacks
def check_genesis_block(bitcoind, genesis_block_hash):
    try:
        yield bitcoind.rpc_getblock(genesis_block_hash)
    except jsonrpc.Error_for_code(-5):
        defer.returnValue(False)
    else:
        defer.returnValue(True)

@defer.inlineCallbacks
def get_subsidy(bitcoind, target):
    res = yield bitcoind.rpc_getblock(target)

    defer.returnValue(res)

nets = dict(
    boostcoin=math.Object(
        P2P_PREFIX='70352205'.decode('hex'),
        P2P_PORT=9697,
        ADDRESS_VERSION=25,
        RPC_PORT=9698,
        RPC_CHECK=defer.inlineCallbacks(lambda bitcoind: defer.returnValue(
            'boostcoinaddress' in (yield bitcoind.rpc_help()) and
            not (yield bitcoind.rpc_getinfo())['testnet']
        )),
        SUBSIDY_FUNC=lambda bitcoind, target: get_subsidy(bitcoind, target),
        BLOCK_PERIOD=60, # s
        SYMBOL='BOST',
        CONF_FILE_FUNC=lambda: os.path.join(os.path.join(os.environ['APPDATA'], 'BoostCoin') if platform.system() == 'Windows' else os.path.expanduser('~/Library/Application Support/BoostCoin/') if platform.system() == 'Darwin' else os.path.expanduser('~/.boostcoin'), 'boostcoin.conf'),
        BLOCK_EXPLORER_URL_PREFIX='http://bost.blockexplorer.cc/block/',
        ADDRESS_EXPLORER_URL_PREFIX='http://bost.blockexplorer.cc/address/',
        TX_EXPLORER_URL_PREFIX='http://bost.blockexplorer.cc/tx/',
        SANE_TARGET_RANGE=(2**256//2**32//1000 - 1, 2**256//2**20 - 1),
        DUMB_SCRYPT_DIFF=1,
        DUST_THRESHOLD=0.001e8,
        CHARITY_ADDRESS='a8b7464a7f9a381cbc578d56c6ffe0fe1f5ebf8e'.decode('hex'),
    ),
    boostcoin_testnet=math.Object(
        P2P_PREFIX='70352205'.decode('hex'),
        P2P_PORT=19697,
        ADDRESS_VERSION=111,
        RPC_PORT=19698,
        RPC_CHECK=defer.inlineCallbacks(lambda bitcoind: defer.returnValue(
            'boostcoinaddress' in (yield bitcoind.rpc_help()) and
            (yield bitcoind.rpc_getinfo())['testnet']
        )),
        SUBSIDY_FUNC=lambda bitcoind, target: get_subsidy(bitcoind, target),
        BLOCK_PERIOD=60, # s
        SYMBOL='BOST',
        CONF_FILE_FUNC=lambda: os.path.join(os.path.join(os.environ['APPDATA'], 'BoostCoin') if platform.system() == 'Windows' else os.path.expanduser('~/Library/Application Support/BoostCoin/') if platform.system() == 'Darwin' else os.path.expanduser('~/.boostcoin'), 'boostcoin.conf'),
        BLOCK_EXPLORER_URL_PREFIX='',
        ADDRESS_EXPLORER_URL_PREFIX='',
        TX_EXPLORER_URL_PREFIX='',
        SANE_TARGET_RANGE=(2**256//2**32//1000 - 1, 2**256//2**20 - 1),
        DUMB_SCRYPT_DIFF=1,
        DUST_THRESHOLD=0.001e8,
        CHARITY_ADDRESS='d84d626ba40cc310f8f1dbf539cac6106d6a4c9c'.decode('hex'),
    ),
)
for net_name, net in nets.iteritems():
    net.NAME = net_name
