from p2pool.bitcoin import networks
from p2pool.util import math

# CHAIN_LENGTH = number of shares back client keeps
# REAL_CHAIN_LENGTH = maximum number of shares back client uses to compute payout
# REAL_CHAIN_LENGTH must always be <= CHAIN_LENGTH
# REAL_CHAIN_LENGTH must be changed in sync with all other clients
# changes can be done by changing one, then the other

nets = dict(
    boostcoin=math.Object(
        PARENT=networks.nets['boostcoin'],
        SHARE_PERIOD=10, # seconds
        CHAIN_LENGTH=12*60*60//10, # shares
        REAL_CHAIN_LENGTH=12*60*60//10, # shares
        TARGET_LOOKBEHIND=30, # shares
        SPREAD=30, # blocks
        IDENTIFIER='2daa910e05c918bc'.decode('hex'),
        PREFIX='ac51b5dba6139470'.decode('hex'),
        P2P_PORT=8697,
        MIN_TARGET=0,
        MAX_TARGET=2**256//2**20 - 1,
        PERSIST=False,
        WORKER_PORT=8698,
        BOOTSTRAP_ADDRS='bost.altmine.net'.split(' '),
        ANNOUNCE_CHANNEL='#p2pool-bost',
        VERSION_CHECK=lambda v: True,
    ),
    boostcoin_testnet=math.Object(
        PARENT=networks.nets['boostcoin_testnet'],
        SHARE_PERIOD=10, # seconds
        CHAIN_LENGTH=12*60*60//10, # shares
        REAL_CHAIN_LENGTH=12*60*60//10, # shares
        TARGET_LOOKBEHIND=30, # shares
        SPREAD=30, # blocks
        IDENTIFIER='70ec5b5902f90ee1'.decode('hex'),
        PREFIX='889d51b98e169789'.decode('hex'),
        P2P_PORT=18697,
        MIN_TARGET=0,
        MAX_TARGET=2**256//2**20 - 1,
        PERSIST=False,
        WORKER_PORT=18698,
        BOOTSTRAP_ADDRS=''.split(' '),
        ANNOUNCE_CHANNEL='',
        VERSION_CHECK=lambda v: True,
    ),
)
for net_name, net in nets.iteritems():
    net.NAME = net_name
